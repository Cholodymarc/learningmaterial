package codewars.century;

public class Century {
    public static void main(String[] args) {
        Century century = new Century();
        System.out.println(century.yearToCentury(1705));
    }

    int yearToCentury(int year){
        int result = year/100;
        if(year%100 > 0){
            result++;
        }
        return result;
    }
}
