package codewars.replacedashwithuppercase;

public class Solution {

    String toCamelCase(String s) {
        String result = s;
        int indexOfUnderscore;
        int indexAfterUnderscore;
        StringBuilder stringBuilder = new StringBuilder();

        indexOfUnderscore = result.indexOf("_");
        indexAfterUnderscore = indexOfUnderscore++;
        stringBuilder.replace(indexOfUnderscore, indexAfterUnderscore, result);
        return result;

    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.toCamelCase("Hello_world"));
    }

}
