package codewars.reversedsequence;

import java.util.Arrays;

public class Sequence {
    public static void main(String[] args) {
        Sequence sequence = new Sequence();
        System.out.println(Arrays.toString(sequence.reverse(10)));
    }

    int[] reverse(int n){
        int[] result = new int[n];

        if(n<=0){
            throw new IllegalArgumentException("n must be grater then zero");
        }

        for(int i = 0, j = n; i < n; i++, j--){
            result[i] = j;
        }

        return result;
    }
}
