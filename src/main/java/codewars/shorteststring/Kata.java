package codewars.shorteststring;

import java.util.Arrays;
import java.util.List;

public class Kata {
    public static int findShort(String s) {

        List<String> words = Arrays.asList(s.split(" "));
        int shortestLength = words.get(0).length();
        for (String word : words) {
            if (word.length() < shortestLength) {
                shortestLength = word.length();
            }
        }
        return shortestLength;
    }

    public static int findShort2(String s) {
        return Arrays.stream(s.split(" ")).mapToInt(String::length).min().orElse(-1);
    }


}
