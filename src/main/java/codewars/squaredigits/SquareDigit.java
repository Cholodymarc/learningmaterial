package codewars.squaredigits;

public class SquareDigit {
    public int squareDigits(int n) {
        // TODO Implement me
        //121 -> 141

        String digits = String.valueOf(n);
        StringBuilder result = new StringBuilder();
        int digit = 0;
        for (int i = 0; i <= digits.length(); i++) {
            digit = (int) digits.charAt(i);
            result.append(String.valueOf(digit * digit));
        }
        return Integer.valueOf(result.toString());
    }

}
