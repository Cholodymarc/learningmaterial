package codewars.strongestevennumber;

public class StrongestEvenNumber {
    public static int strongestEven(int n, int m) {
        int maxDivisionsCount = 1;
        for (int number = n; number <= m; number++) {
            if (number == 1) {
                maxDivisionsCount = 1;
            } else {
                int tempDivisionsCount = 1;
                for (int j = 0; j <= m; j++) {
                    if ((number / 2) % 2 == 0) {
                        tempDivisionsCount++;
                    } else {
                        break;
                    }
                    if (tempDivisionsCount > maxDivisionsCount) {
                        maxDivisionsCount = tempDivisionsCount;
                    }
                }
            }
        }
        return maxDivisionsCount;
    }

    public static void main(String[] args) {
        System.out.println(strongestEven(5, 10));
    }
}
