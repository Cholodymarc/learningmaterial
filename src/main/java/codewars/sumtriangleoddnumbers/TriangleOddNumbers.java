package codewars.sumtriangleoddnumbers;

public class TriangleOddNumbers {
    public static int rowSumOddNumbers(int n) {
        int result = 1;
        if (n > 1) {
            for (int i = 1; i < n; i++) {
                result += 2 * i;
            }
            int temp = result;
            for (int i = 1; i < n; i++) {
                temp += 2;
                result += temp;
            }
        }
        return result;
    }

    public static int rowSumOddNumbers2(int n) {
        return n * n * n;
    }

    public static long[] oddRow(int n) {
        long[] result = new long[n];
        long firstValueInARow = 1;
        if (n == 1) {
            result[0] = firstValueInARow;
            return result;
        }
        //find first value in a row
        for (int i = 1; i < n; i++) {
            firstValueInARow += 2 * i;
        }
        //add numbers to result table
        for (int i = 0; i < n; i++) {
            result[i] = firstValueInARow;
            firstValueInARow += 2;
        }
        return result;
    }

}
