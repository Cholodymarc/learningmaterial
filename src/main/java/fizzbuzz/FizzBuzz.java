package fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {
    List<String> printNumbers(int limit) {
        List<String> resultList = new ArrayList<>();
        String result;
        for (int number = 1; number <= limit; number++) {
            result = "";
            if (number % 3 == 0) {
                result += "FIZZ";
            }
            if (number % 5 == 0) {
                result += "BUZZ";
            }
            if ("".equals(result)) {
                result = String.valueOf(number);
            }
            resultList.add(result);
        }
        return resultList;
    }

    List<String> fizzBuzz(int limit) {
        List<String> resultList = new ArrayList<>();
        for (int number = 1; number <= limit; number++) {
            if (number % 3 == 0 && number % 5 == 0) {
                resultList.add("FIZZBUZZ");
            } else if (number % 3 == 0) {
                resultList.add("FIZZ");
            } else if (number % 5 == 0) {
                resultList.add("BUZZ");
            } else {
                resultList.add(String.valueOf(number));
            }
        }
        return resultList;
    }
}
