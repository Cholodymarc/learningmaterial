package gradebook;

import java.util.ArrayList;
import java.util.List;

public class GradeBook {
    private List<Subject> subjects;

    GradeBook() {
        subjects = new ArrayList<>();
    }

    void addSubject(Subject subject){
        if(subjects.contains(subject)){
            throw new IllegalStateException();
        }
        subjects.add(subject);
    }

    List<Subject> getSubjects() {
        return subjects;
    }

    double calculateAverage(){
        List<Double> subjectAverages = new ArrayList<>();
        for(Subject subject : subjects){
            double subjectAverage = 0;
            for(Double grade : subject.getGrades()){
                subjectAverage += grade;
            }
            subjectAverages.add(subjectAverage/subject.getGrades().size());
        }
        double average = 0;

        for(Double subjectAverage : subjectAverages){
            average += subjectAverage;
        }
        return average/subjectAverages.size();
    }
}
