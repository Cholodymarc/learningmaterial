package gradebook;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private String name;
    private List<Double> grades;

    public Subject(String name) {
        this.name = name;
        grades = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Double> getGrades() {
        return grades;
    }

    void addGrade(double grade){
        grades.add(grade);
    }

    double calculateAverage(){
        double average = 0;
        for(Double grade : grades){
            average += grade;
        }
        return average/grades.size();
    }
}
