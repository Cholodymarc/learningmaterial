package greeting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Greeting {

    String greet(String[] names) {
        List<String> namesLowerCase = new ArrayList<>();
        List<String> namesUpperCase = new ArrayList<>();

        boolean isNameListEmpty = !separateNames(names, namesLowerCase, namesUpperCase);
        if (isNameListEmpty) {
            return "Hello, my friend.";
        }

        String lowerCaseResult = generateResult(false, namesLowerCase);
        String upperCaseResult = generateResult(true, namesUpperCase);

        if (!lowerCaseResult.equals("") && !upperCaseResult.equals("")) {
            upperCaseResult = new StringBuilder(upperCaseResult)
                    .replace(5, 6, "")
                    .replace(upperCaseResult.length() - 2, upperCaseResult.length() - 1, "!")
                    .toString();
            return new StringBuilder(lowerCaseResult)
                    .append(" AND ")
                    .append(upperCaseResult)
                    .toString();
        }
        return new StringBuilder(lowerCaseResult)
                .append(upperCaseResult)
                .toString();
    }

    private boolean separateNames(String[] names, List<String> namesLowerCase, List<String> namesUpperCase) {
        for (String name : names) {
            if (name == null) {
                return false;
            }
            List<String> splitNames = Arrays.asList(name.split(", "));

            if (name.toUpperCase().equals(name)) {
                namesUpperCase.addAll(splitNames);
            } else {
                namesLowerCase.addAll(splitNames);
            }
        }
        return true;
    }

    private String generateResult(boolean isUpperCase, List<String> names) {
        boolean HelloFlag = true;
        String hello = "Hello";
        String and = " and ";
        String result = "";

        if (isUpperCase) {
            hello = hello.toUpperCase();
            and = and.toUpperCase();
        }
        for (int index = 0; index < names.size(); index++) {
            if (HelloFlag) {
                result = new StringBuilder(result).append(hello).toString();
                HelloFlag = false;
            }
            if (names.size() == 1 || index <= names.size() - 2) {
                result = new StringBuilder(result).append(", ").toString();
            }
            result = new StringBuilder(result).append(names.get(index)).toString();
            if (index == names.size() - 2) {
                result = new StringBuilder(result).append(and).toString();
            }
            if (index == names.size() - 1) {
                result = new StringBuilder(result).append(".").toString();
            }
        }
        return result;
    }
}

