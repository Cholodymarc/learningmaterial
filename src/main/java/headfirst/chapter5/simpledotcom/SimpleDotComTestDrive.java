package headfirst.chapter5.simpledotcom;

import java.util.ArrayList;

public class SimpleDotComTestDrive {
    public static void main(String[] args) {
        int numberOfGuesses = 0;
        GameHelper helper = new GameHelper();

        DotCom theDotCom = new DotCom();
        int randomNum = (int) (Math.random() * 5);

        String s1 = String.valueOf(randomNum);
        String s2 = String.valueOf(randomNum + 1);
        String s3 = String.valueOf(randomNum + 2);
        ArrayList<String> locations = new ArrayList<>();
        locations.add(s1);
        locations.add(s2);
        locations.add(s3);
        theDotCom.setLocationCells(locations);
        boolean isAlive = true;

        while (isAlive) {
            String guess = helper.getUserInput("enter a number");
            String result = theDotCom.checkYourself(guess);
            numberOfGuesses++;
            if (result.equals("kill")) {
                isAlive = false;
                System.out.println("You took " + numberOfGuesses + " guesses");
            }
        }
    }
}
