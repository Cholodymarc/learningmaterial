package headfirst.chapter8.abstractexample;

abstract class Animal {
    abstract void roam();
}
