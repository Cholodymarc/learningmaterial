package headfirst.chapter8.abstractexample;

public class AnimalTestDrive {
    public static void main(String[] args) {
        MyAnimalList animals = new MyAnimalList();
        Dog a = new Dog();
        Cat c = new Cat();
        animals.add(a);
        animals.add(c);
        System.out.println(a.equals(c));
        System.out.println(a.hashCode());
        System.out.println(c.hashCode());
        System.out.println(a.getClass());
        System.out.println(c.getClass());
        System.out.println(a.toString());
        System.out.println(c.toString());
    }
}
