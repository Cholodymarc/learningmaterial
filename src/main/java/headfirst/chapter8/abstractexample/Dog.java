package headfirst.chapter8.abstractexample;

public class Dog extends Canine implements Pet {
    @Override
    public void roam() {
        super.roam();
    }

    void eat(){
        //dog specific eat
    }

    @Override
    public void beFriendly() {

    }

    @Override
    public void play() {

    }
}
