package headfirst.chapter8.abstractexample;

abstract public class Feline extends Animal {
    abstract void roam();
}
