package headfirst.chapter8.abstractexample;

public class MakeCanine {
    public void go(){
        Canine c;
        c = new Dog();
//        c = new Canine(); ' cannot do this, Canine is abstract
        c.roam();

        Object dog = new Dog();
        //dog.roam();

        //dog.eat(); // cannot do that, method eat not declared in Object
        if(dog instanceof Dog){
            Dog d = (Dog) dog;
            d.eat(); //can do that, type cast
        }
    }

    public static void main(String[] args) {
        MakeCanine makeCanine = new MakeCanine();
        makeCanine.go();
    }
}
