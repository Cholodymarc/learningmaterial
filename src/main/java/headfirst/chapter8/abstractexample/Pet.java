package headfirst.chapter8.abstractexample;

public interface Pet {
    void beFriendly();

    void play();

}
