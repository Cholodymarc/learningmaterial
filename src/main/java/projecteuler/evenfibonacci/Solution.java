package projecteuler.evenfibonacci;

public class Solution {
    public static void main(String[] args) {

        long fibNumber = 0;
        long result = 2;
        long prev = 2;
        long prevprev =1;
        while(fibNumber <= 4_000_000) {
            fibNumber = prev + prevprev;
            prevprev = prev;
            prev = fibNumber;
            if(fibNumber%2==0){
                System.out.println(fibNumber);
                result+=fibNumber;
            }
        }
        System.out.println(result);
    }
}
