package simpleprograms.fibonacci;

public class Fibonacci {
    //1,1,2,3,5,8,13,21

    long fibonacci1(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n cannot be lower than zero");
        }
        if(n==0){
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibonacci1(n - 1) + fibonacci1(n - 2);
    }

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci();
//        fibonacci.fibonacci1(-7);
        System.out.println(fibonacci.fibonacci1(0));
        System.out.println(fibonacci.fibonacci1(1));
        System.out.println(fibonacci.fibonacci1(2));
        System.out.println(fibonacci.fibonacci1(3));
        System.out.println(fibonacci.fibonacci1(4));
        System.out.println(fibonacci.fibonacci1(5));
        System.out.println(fibonacci.fibonacci1(6));
        System.out.println(fibonacci.fibonacci1(7));
        System.out.println(fibonacci.fibonacci1(8));
    }
}
