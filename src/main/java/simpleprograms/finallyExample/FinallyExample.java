package simpleprograms.finallyExample;

public class FinallyExample {
    public static void main(String[] args) {
        FinallyExample finallyExample = new FinallyExample();
        System.out.println(finallyExample.test(10,5));
    }

    int test(int a, int b){
        try{
            throw new RuntimeException("exception from method");
//            return a+b;
        } finally {
            throw new RuntimeException("exception from finally");
//            return a-b; //bad practice to return something from finally
        }
    }
}
