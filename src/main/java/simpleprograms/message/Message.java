package simpleprograms.message;

public class Message {
    private String content;

    public Message(String message) {
        this.content = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "content='" + content + '\'' +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(new Message("THANK YOU :):):)"));
    }
}
