package udemy.networking;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerExample {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(6000)) {

            while (true) {
                new Echoer(serverSocket.accept()).start();

            }
        } catch (IOException e) {
            System.out.println("Server exception " + e.getMessage());
        }
    }
}

//                Socket socet = serverSocket.accept();
//                System.out.println("Client connected");
//                BufferedReader input = new BufferedReader(
//                        new InputStreamReader(socet.getInputStream()));
//                PrintWriter output = new PrintWriter(socet.getOutputStream(), true);
//                String echoString = input.readLine();
//                try{
//                    Thread.sleep(15000);
//                } catch (InterruptedException e){
//                    System.out.println("Thread interrupted " + e.getMessage());
//                }
//
//                if (echoString.equals("exit")) {
//                    break;
//                }
//                output.println("echo from server: " + echoString);