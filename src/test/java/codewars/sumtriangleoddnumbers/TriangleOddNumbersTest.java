package codewars.sumtriangleoddnumbers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TriangleOddNumbersTest {

    @Test
    public void testFirstSolution() {
        assertEquals(1, TriangleOddNumbers.rowSumOddNumbers(1));
        assertEquals(8, TriangleOddNumbers.rowSumOddNumbers(2));
        assertEquals(27, TriangleOddNumbers.rowSumOddNumbers(3));
        assertEquals(74088, TriangleOddNumbers.rowSumOddNumbers(42));
    }

    @Test
    public void testSecondSolution(){
        assertEquals(1, TriangleOddNumbers.rowSumOddNumbers2(1));
        assertEquals(8, TriangleOddNumbers.rowSumOddNumbers2(2));
        assertEquals(27, TriangleOddNumbers.rowSumOddNumbers2(3));
        assertEquals(74088, TriangleOddNumbers.rowSumOddNumbers2(42));
    }

}