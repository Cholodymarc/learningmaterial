package fizzbuzz;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest {
    private FizzBuzz fizzBuzz;

    @BeforeEach
    void setUp(){
        fizzBuzz = new FizzBuzz();
    }

    @Test
    void printNumbers() {
        assertEquals(List.of("1","2","FIZZ"), fizzBuzz.printNumbers(3));
        assertEquals(List.of("1","2","FIZZ","4","BUZZ"), fizzBuzz.printNumbers(5));
        assertEquals(List.of("1","2","FIZZ","4","BUZZ","FIZZ","7","8","FIZZ","BUZZ","11","FIZZ","13","14","FIZZBUZZ"), fizzBuzz.printNumbers(15));

        assertEquals(List.of("1","2","FIZZ"), fizzBuzz.fizzBuzz(3));
        assertEquals(List.of("1","2","FIZZ","4","BUZZ"), fizzBuzz.fizzBuzz(5));
        assertEquals(List.of("1","2","FIZZ","4","BUZZ","FIZZ","7","8","FIZZ","BUZZ","11","FIZZ","13","14","FIZZBUZZ"), fizzBuzz.fizzBuzz(15));
    }
}