package gradebook;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GradeBookTest {
    private GradeBook gradeBook;
    private Subject math;
    private Subject biology;

    @BeforeEach
    void beforeEach(){
        setUp();
        actions();
    }

    private void setUp() {
        gradeBook = new GradeBook();
        math = new Subject("Math");
        biology = new Subject("Biology");
    }

    private void actions() {
        gradeBook.addSubject(math);
        gradeBook.addSubject(biology);
        math.addGrade(4d);
        math.addGrade(5d);
        biology.addGrade(4);
    }

    @Test
    void addSubject() {
        List<Subject> testSubjects = List.of(new Subject("Math"), new Subject("Biology"));

        assertEquals(testSubjects.size(), gradeBook.getSubjects().size());
        for (int index = 0; index < testSubjects.size(); index++) {
            assertEquals(testSubjects.get(index).getName(), gradeBook.getSubjects().get(index).getName());
        }
    }

    @Test
    void addExistingSubject(){
        assertThrows(IllegalStateException.class, () -> gradeBook.addSubject(math));
    }

    @Test
    void addGrade(){
        List<Double> testGrades = List.of(4d);
        for (int index = 0; index < testGrades.size(); index++){
            assertEquals(testGrades.get(index),math.getGrades().get(index));
        }
    }

    @Test
    void calculateSubjectAverage(){
        double mathAverage = 4.5d;
        assertEquals(mathAverage,gradeBook.getSubjects().get(0).calculateAverage());

        double biologyAverage = 4.0d;
        assertEquals(biologyAverage, gradeBook.getSubjects().get(1).calculateAverage());
    }

    @Test
    void calculateGradeBookAverage(){
        double testAverage = 4.25d;
        assertEquals(testAverage,gradeBook.calculateAverage());
    }


}