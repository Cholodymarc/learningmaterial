package greeting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GreetingTest {
    private Greeting greeting;

    @BeforeEach
    void setUp(){
        greeting = new Greeting();
    }

    @ParameterizedTest
    @MethodSource("methodSource")
    void greetingCheck(String message, String[] name){
        assertEquals(message, greeting.greet(name));
    }

    private static Stream<Arguments> methodSource(){
        String[] name1 = {"Bob"};
        String[] name2 = {null};
        String[] name3 = {"JERRY"};
        String[] name4 = {"Jane", "Jerry"};
        String[] name5 = {"JANE", "JERRY"};
        String[] name6 = {"Amy", "Brian", "Charlotte"};
        String[] name7 = {"Amy", "BRIAN", "Charlotte"};
        String[] name8 = {"Bob", "Charlie, Dianne"};

        return Stream.of(Arguments.of("Hello, Bob.",name1),
                Arguments.of("Hello, my friend.",name2),
                Arguments.of("HELLO, JERRY.",name3),
                Arguments.of("Hello, Jane and Jerry.",name4),
                Arguments.of("HELLO, JANE AND JERRY.",name5),
                Arguments.of("Hello, Amy, Brian and Charlotte.",name6),
                Arguments.of("Hello, Amy and Charlotte. AND HELLO BRIAN!",name7),
                Arguments.of("Hello, Bob, Charlie and Dianne.",name8)
                );
    }
}