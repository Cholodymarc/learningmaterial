package simpleprograms.fibonacci;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciTest {
    private Fibonacci fibonacci;

    @Before
    public void setUp(){
            fibonacci = new Fibonacci();
    }

    @Test
    public void testFibonacci1(){
        assertEquals(0, fibonacci.fibonacci1(0));
        assertEquals(1, fibonacci.fibonacci1(1));
        assertEquals(1, fibonacci.fibonacci1(2));
        assertEquals(2, fibonacci.fibonacci1(3));
        assertEquals(3, fibonacci.fibonacci1(4));
        assertEquals(5, fibonacci.fibonacci1(5));
        assertEquals(8, fibonacci.fibonacci1(6));
        assertEquals(13, fibonacci.fibonacci1(7));
        assertEquals(21, fibonacci.fibonacci1(8));
        assertEquals(34, fibonacci.fibonacci1(9));
    }
}